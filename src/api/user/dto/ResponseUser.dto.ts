import { IsEmail, IsNotEmpty, IsNumber, IsString, Length } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class ResponseUserDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    id: number

    @ApiProperty({ minLength: 1, maxLength: 255 })
    @IsString()
    @Length(1, 255)
    username: string

    @ApiProperty({ minLength: 1, maxLength: 255, type: "email" })
    @IsString()
    @IsEmail()
    @Length(1, 255)
    email: string
}