import { ResponseUserDTO } from './dto/ResponseUser.dto';
import { AuthGuard } from '../../auth/guard/auth.guard';
import { UserService } from './user.service';
import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';

@ApiTags('User')
@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('users')
  @ApiOkResponse({ type: [ResponseUserDTO] })
  @UseGuards(AuthGuard)
  getAllUsers() {
    return this.userService.getAllUsers();
  }

  @Get('user')
  @ApiOkResponse({ type: ResponseUserDTO })
  findByName(@Query('username') username: any) {
    return this.userService.findOneByUsername(username);
  }
}
