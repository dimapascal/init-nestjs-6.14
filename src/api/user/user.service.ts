import { RegisterUserDTO } from '../../auth/dto/RegisterUser.dto';
import { LoginUserDTO } from '../../auth/dto/LoginUser.dto';
import { ResponseUserDTO } from './dto/ResponseUser.dto';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserEntity } from '../../entity/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { encryptPassword, comparePasswordWithHash } from 'src/auth/bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userEntity: Repository<UserEntity>,
  ) {}

  async createUser(registerUserDTO: RegisterUserDTO): Promise<ResponseUserDTO> {
    const user = {
      ...registerUserDTO,
      password: encryptPassword(registerUserDTO.password),
    };
    return await this.userEntity.save(user);
  }
  async getAllUsers(): Promise<Array<ResponseUserDTO>> {
    const users = await this.userEntity.find();
    return users || [];
  }
  async findOneByUsername(username: string): Promise<ResponseUserDTO> {
    return await this.userEntity.findOne({ username });
  }
  async findOneByEmail(email: string): Promise<ResponseUserDTO> {
    return await this.userEntity.findOne({ email });
  }
  async findByEmailAndPassword(
    loginUserDTO: LoginUserDTO,
  ): Promise<ResponseUserDTO> {
    const user = await this.userEntity
      .createQueryBuilder('user')
      .select(['user.password', 'user.id'])
      .where('user.email = :email', { email: loginUserDTO.email })
      .getOne();
    if (!user) {
      throw new UnauthorizedException();
    }
    const compareResult = comparePasswordWithHash({
      password: loginUserDTO.password,
      hash: user.password,
    });
    if (compareResult) {
      return await this.userEntity.findOne(user.id);
    }
    throw new UnauthorizedException();
  }
}
