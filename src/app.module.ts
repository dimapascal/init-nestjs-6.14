import { ApiModule } from './api/api.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { DatabaseModule } from './config/database.module';
import { MailModule } from './mail/mail.module';
import { MigrationModule } from './migration/migration.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    DatabaseModule,
    ConfigModule,
    AuthModule,
    MigrationModule,
    ApiModule,
    MailModule,
  ],
})
export class AppModule {}
