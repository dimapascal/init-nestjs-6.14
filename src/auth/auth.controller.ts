import { ResponseUserDTO } from '../api/user/dto/ResponseUser.dto';
import { JwtResponseDTO } from './dto/JwtResponse.dto';
import { LoginUserDTO } from './dto/LoginUser.dto';
import { RegisterUserDTO } from './dto/RegisterUser.dto';
import { AuthService } from './auth.service';
import {
  Controller,
  Body,
  Post,
  Get,
  Param,
  BadRequestException,
  UnauthorizedException,
} from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @ApiOkResponse({ type: JwtResponseDTO })
  async login(
    @Body() loginUserDTO: LoginUserDTO,
  ): Promise<JwtResponseDTO | UnauthorizedException> {
    return await this.authService.login(loginUserDTO);
  }

  @Post('register')
  @ApiOkResponse({ type: ResponseUserDTO })
  async register(
    @Body() registerUserDTO: RegisterUserDTO,
  ): Promise<ResponseUserDTO | BadRequestException> {
    return await this.authService.register(registerUserDTO);
  }

  @Get('refresh-token/:token')
  @ApiOkResponse({ type: JwtResponseDTO })
  async refreshToken(
    @Param('token') token: string,
  ): Promise<JwtResponseDTO | UnauthorizedException> {
    return await this.authService.refreshToken(token);
  }
}
