import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ConfigModule } from './../config/config.module';
import { MailModule } from './../mail/mail.module';
import { Module } from '@nestjs/common';
import { UserModule } from '../api/user/user.module';

@Module({
  imports: [UserModule, ConfigModule, MailModule],
  providers: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
