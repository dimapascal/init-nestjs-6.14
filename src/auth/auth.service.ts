import * as jwt from 'jsonwebtoken';

import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';

import { ConfigService } from './../config/config.service';
import { JwtPayloadDTO } from './dto/JwtPayload.dto';
import { JwtResponseDTO } from './dto/JwtResponse.dto';
import { LoginUserDTO } from './dto/LoginUser.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { RegisterUserDTO } from './dto/RegisterUser.dto';
import { UserService } from '../api/user/user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly configService: ConfigService,
    private readonly mailerService: MailerService,
  ) {}

  async login(loginUserDTO: LoginUserDTO) {
    const user = await this.userService.findByEmailAndPassword(loginUserDTO);
    if (user) {
      return this.generateTokens(user);
    }
    throw new UnauthorizedException();
  }
  async register(registerUserDTO: RegisterUserDTO) {
    const emailExit = await this.userService.findOneByEmail(
      registerUserDTO.email,
    );
    if (emailExit) {
      throw new BadRequestException('User with same email already exist');
    }
    const user = await this.userService.createUser(registerUserDTO);
    if (user) {
      this.mailerService.sendMail({
        to: user.email,
        subject: 'Success Registration',
        template: 'registration',
        context: user,
      });
      return user;
    }
    throw new BadRequestException('Registration failed');
  }

  async refreshToken(token: string) {
    const data: any = this._decodeRefreshToken(token);
    if (typeof data == 'string') {
      throw new BadRequestException(data);
    }
    if (data && data.user) {
      return this.generateTokens(data.user);
    }
  }

  generateTokens(data: JwtPayloadDTO): JwtResponseDTO {
    const accessToken = this._createAccessToken(data);
    const refreshToken = this._createRefreshToken(accessToken);
    return {
      expiresIn: this.configService.accessTokenExpireIn,
      accessToken,
      refreshToken,
    };
  }

  private _createRefreshToken(accessToken: string): string {
    const { token, expiresIn } = this.configService.refreshTokenBlock;
    return jwt.sign({ token: accessToken }, token, { expiresIn });
  }

  private _createAccessToken(user: JwtPayloadDTO): string {
    const { token, expiresIn } = this.configService.accessTokenBlock;
    return jwt.sign({ user }, token, { expiresIn });
  }

  private _decodeRefreshToken(token: string): object | string {
    try {
      const validationResp: any = jwt.verify(
        token,
        this.configService.refreshToken,
      );
      if (validationResp && validationResp.token) {
        return jwt.decode(validationResp.token);
      }
      return {};
    } catch (error) {
      return error.message;
    }
  }
}
