import * as bcrypt from 'bcrypt'

export function encryptPassword(password: string): string {
    const { BCRYPT_SALT_ROUNDS, BCRYPT_PLAINTEXT_PASSWORD } = process.env
    const salt = bcrypt.genSaltSync(parseInt(BCRYPT_SALT_ROUNDS) || 10);
    const hash = bcrypt.hashSync(BCRYPT_PLAINTEXT_PASSWORD, salt);
    return bcrypt.hashSync(password, hash);
}


export function comparePasswordWithHash({ password, hash }) {
    return bcrypt.compareSync(password, hash);
}