import { IsEmail, IsNotEmpty, IsNumber } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class JwtPayloadDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    id: number

    @ApiProperty({ type: 'email' })
    @IsNotEmpty()
    @IsEmail()
    email: string
}