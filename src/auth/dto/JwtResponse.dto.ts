import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class JwtResponseDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumberString()
    expiresIn: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    refreshToken: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    accessToken: string
}