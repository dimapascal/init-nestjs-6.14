import { IsEmail, IsString, Length } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class RegisterUserDTO {
    @ApiProperty({ minLength: 1, maxLength: 255, })
    @IsString()
    @Length(1, 255)
    username: string

    @ApiProperty({ minLength: 1, maxLength: 255, type: 'email' })
    @IsEmail()
    @Length(1, 255)
    email: string

    @ApiProperty({ minLength: 1, maxLength: 255, })
    @IsString()
    @Length(1, 255)
    password: string
}