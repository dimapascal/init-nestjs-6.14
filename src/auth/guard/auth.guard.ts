import * as jwt from 'jsonwebtoken'

import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';

import { ExtractJwt } from 'passport-jwt';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const req = context.switchToHttp().getRequest();
        const token = this.validateBearer(req)
        if (!token) {
            throw new UnauthorizedException()
        }
        try {
            const data: any = this.validateToken(token);
            req['user'] = data['user']
            return true
        } catch (error) {
            throw new UnauthorizedException()
        }
    }

    validateBearer(req): string {
        return ExtractJwt.fromAuthHeaderAsBearerToken()(req);
    }

    validateToken(token: string) {
        return jwt.verify(token, process.env.AUTH_SECRET_KEY)
    }
}
