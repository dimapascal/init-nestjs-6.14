import * as Joi from '@hapi/joi';
import * as dotenv from 'dotenv';
import * as dotenvParseVariables from 'dotenv-parse-variables';
import * as path from 'path';

import { Injectable } from '@nestjs/common';

export interface EnvConfig {
  PORT: number;

  DB_TYPE: string;
  DB_HOST: string;
  DB_PORT: number;
  DB_USERNAME: string;
  DB_PASSWORD: string;
  DB_NAME: string;
  DB_ENTITIES: string[];
  DB_AUTO_LOAD_ENTITIES: boolean;
  DB_LOGGING: boolean;
  DB_SYNCHRONIZE: boolean;
  DB_MIGRATION: boolean;

  AUTH_EXPIRES_IN: string;
  AUTH_SECRET_KEY: string;
  AUTH_REFRESH_EXPIRE_IN: string;
  AUTH_REFRESH_SECRET_KEY: string;

  BCRYPT_SALT_ROUNDS: number;
  BCRYPT_PLAINTEXT_PASSWORD: string;

  MAIL_HOST: string;
  MAIL_PORT: number;
  MAIL_SECURE: boolean;
  MAIL_USERNAME: string;
  MAIL_PASSWORD: string;
}

export interface DatabaseConfig {
  readonly database: string;
  readonly entities: string[];
  readonly host: string;
  readonly password: string;
  readonly port: number;
  readonly synchronize: boolean;
  readonly type: 'postgres';
  readonly username: string;
}

export interface TokenConfig {
  readonly token: string;
  readonly expiresIn: string;
}

export interface MailingConfig {
  host: string;
  port: number;
  secure: boolean;
  auth: {
    user: string;
    pass: string;
  };
}

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;
  constructor() {
    const dotenvPath = path.join(__dirname, '../../.env');
    const dotenvEnv = dotenv.config({ path: dotenvPath });
    if (dotenvEnv.error) {
      throw dotenvEnv.error;
    }
    const config = dotenvParseVariables(dotenvEnv.parsed);
    const updatedConfigs = {
      ...config,
      DB_ENTITIES: config.DB_ENTITIES.filter(item => item),
    };
    this.envConfig = this.validateVariables(updatedConfigs);
  }

  private get _joiSchema(): Joi.ObjectSchema {
    return Joi.object({
      PORT: Joi.number().default(3000),

      DB_TYPE: Joi.string().required(),
      DB_HOST: Joi.string().required(),
      DB_PORT: Joi.number().default(5432),
      DB_USERNAME: Joi.string().required(),
      DB_PASSWORD: Joi.string().required(),
      DB_NAME: Joi.string().required(),
      DB_ENTITIES: Joi.array()
        .items(Joi.string().required())
        .required(),
      DB_AUTO_LOAD_ENTITIES: Joi.boolean().required(),
      DB_LOGGING: Joi.boolean().required(),
      DB_SYNCHRONIZE: Joi.boolean().required(),
      DB_MIGRATION: Joi.boolean().required(),

      AUTH_EXPIRES_IN: Joi.string().required(),
      AUTH_SECRET_KEY: Joi.string().required(),
      AUTH_REFRESH_EXPIRE_IN: Joi.string().required(),
      AUTH_REFRESH_SECRET_KEY: Joi.string().required(),

      BCRYPT_SALT_ROUNDS: Joi.number().default(10),
      BCRYPT_PLAINTEXT_PASSWORD: Joi.string().required(),

      MAIL_HOST: Joi.string().required(),
      MAIL_PORT: Joi.number().default(587),
      MAIL_SECURE: Joi.boolean().required(),
      MAIL_USERNAME: Joi.string().required(),
      MAIL_PASSWORD: Joi.string().required(),
    });
  }

  private validateVariables(envConfig: any): any {
    const { error, value } = this._joiSchema.validate(envConfig);
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return value;
  }

  get mailing(): MailingConfig {
    const data = {
      host: this.envConfig.MAIL_HOST,
      port: this.envConfig.MAIL_PORT,
      secure: this.envConfig.MAIL_SECURE,
      auth: {
        user: this.envConfig.MAIL_USERNAME,
        pass: this.envConfig.MAIL_PASSWORD,
      },
    };
    return data;
  }

  get database(): DatabaseConfig {
    return {
      type: 'postgres',
      host: this.envConfig.DB_HOST,
      port: Number(this.envConfig.DB_PORT),
      username: this.envConfig.DB_USERNAME,
      password: this.envConfig.DB_PASSWORD,
      database: this.envConfig.DB_NAME,
      entities: this.envConfig.DB_ENTITIES,
      synchronize: Boolean(this.envConfig.DB_SYNCHRONIZE),
    };
  }
  get refreshTokenBlock(): TokenConfig {
    return {
      token: this.envConfig.AUTH_REFRESH_SECRET_KEY,
      expiresIn: this.envConfig.AUTH_REFRESH_EXPIRE_IN,
    };
  }
  get accessTokenBlock(): TokenConfig {
    return {
      token: this.envConfig.AUTH_SECRET_KEY,
      expiresIn: this.envConfig.AUTH_EXPIRES_IN,
    };
  }
  get accessTokenExpireIn(): string {
    return this.envConfig.AUTH_EXPIRES_IN;
  }
  get port(): number {
    return this.envConfig.PORT;
  }
  get refreshToken(): string {
    return this.envConfig.AUTH_REFRESH_SECRET_KEY;
  }
  get migration(): boolean {
    return this.envConfig.DB_MIGRATION;
  }
  get synchronize(): boolean {
    return this.envConfig.DB_SYNCHRONIZE;
  }
}
export default new ConfigService();
