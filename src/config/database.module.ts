import { ConfigModule } from './config.module';
import { ConfigService } from './config.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule,],
            useFactory: async (configService: ConfigService) =>
                configService.database,
            inject: [ConfigService,],
        }),
    ],
})
export class DatabaseModule { }
