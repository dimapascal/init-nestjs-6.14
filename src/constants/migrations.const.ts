import { UserEntity } from '../entity/user.entity';
export const USERS: Array<UserEntity> = [
    {
        id: 1,
        email: 'firstemail@gmail.com',
        username: 'firstUser',
        active: true,
        password: '1111'
    },
    {
        id: 2,
        email: 'secoundemil@gmail.com',
        username: 'secondUser',
        active: true,
        password: '1111'
    },
    {
        id: 3,
        email: 'thirdemail@gmail.com',
        username: 'thirdUser',
        active: true,
        password: '1111'
    },
    {
        id: 4,
        email: 'forthemail@gmail.com',
        username: 'forthUser',
        active: false,
        password: '1111'
    },
]