import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';

export class SuccessDTO {
    @ApiProperty()
    @IsBoolean()
    success: boolean
}