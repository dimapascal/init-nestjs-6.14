import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'character varying', unique: true, })
  email: string

  @Column({ type: 'character varying' })
  username: string;

  @Column({ type: "boolean", default: true })
  active: boolean

  @Column({
    type: 'character varying',
    select: false,
  })
  password: string
}