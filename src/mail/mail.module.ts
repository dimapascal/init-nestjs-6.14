import * as path from 'path';

import { HandlebarsAdapter, MailerModule } from '@nestjs-modules/mailer';

import ConfigService from './../config/config.service';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: ConfigService.mailing,
      defaults: {
        from: 'dimapascal@gmail.com',
      },
      template: {
        dir: path.join(__dirname, '/templates'),
        adapter: new HandlebarsAdapter(),
        options: { strict: true },
      },
    }),
  ],
})
export class MailModule {}
