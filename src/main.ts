import 'dotenv/config';

import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { GlobalValidationPipe } from './pipe/Validation.pipe';
import { Logger, } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

const port = process.env.PORT || 8080;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new GlobalValidationPipe());

  app.enableCors();

  app.setGlobalPrefix('api');

  const options = new DocumentBuilder()
    .setTitle('NestJs init project')
    .setDescription('The project API docs')
    .setVersion('1.0')
    .build();

  const catDocument = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs', app, catDocument);

  await app.listen(port);
  Logger.log('App started on port ' + port, 'Bootstrap');
}
bootstrap();
