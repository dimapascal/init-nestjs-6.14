import { ConfigModule } from './../config/config.module';
import { MigrationService } from './migration.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './../entity/user.entity';

@Module({
  providers: [MigrationService],
  imports: [TypeOrmModule.forFeature([UserEntity]), ConfigModule],
})
export class MigrationModule { }
