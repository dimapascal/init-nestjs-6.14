import { ConfigService } from './../config/config.service';
import { UserEntity } from './../entity/user.entity';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import * as migrationsConstants from '../constants/migrations.const'
import { encryptPassword } from 'src/auth/bcrypt';


@Injectable()
export class MigrationService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userEntity: Repository<UserEntity>,
        private readonly configService: ConfigService,
    ) {
        if (this.configService.migration) {
            this.initMigrations()
        }
    }


    async initMigrations() {
        const users = await this._migrateUsers()
        if (users) {
            Logger.log("Database migration success", 'Migration');
        }
    }

    private async _migrateUsers() {
        const users = migrationsConstants.USERS.map(user => ({
            ...user,
            password: encryptPassword(user.password)
        }))
        const response = await this.userEntity.save(users)
        return !!response
    }
}
