import { BadRequestException, Injectable, ValidationPipe } from '@nestjs/common';

import { ValidationError } from 'class-validator';

@Injectable()
export class GlobalValidationPipe extends ValidationPipe {
    exceptionFactory = (errors: ValidationError[]) => {
        let error = errors[0];
        while (error.children.length) {
            error = error.children[0];
        }
        const errorMessage = error.constraints[Object.keys(error.constraints)[0]];
        throw new BadRequestException(errorMessage);
    }
}
